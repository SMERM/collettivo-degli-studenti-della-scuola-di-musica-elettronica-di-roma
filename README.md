# Collettivo degli Studenti della Scuola di Musica Elettronica di Roma

In questo repository vengono riportati ed archiviati tutti gli elementi inerenti il Collettivo degli Studenti della Scuola di Musica Elettronica di Roma.

## Che cos'è il Collettivo degli Studenti della Scuola di Musica Elettronica di Roma

Esso è il luogo di scambio di idee degli studenti della Scuola di Musica Elettronica di Roma, il cui obiettivo è il dialogo degli studenti con i docenti in maniera chiara e diretta.

## Perchè questo repository?

Questo repository è stato realizzato per esprimere il massimo della chiarezza e trasparenza riguardante le decisioni degli studenti e per gli studenti.

## Cosa si può trovare in questo repository?

- [Verbali delle riunioni del Collettivo](/verbali_delle_riunioni)
