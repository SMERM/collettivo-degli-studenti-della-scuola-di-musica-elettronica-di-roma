# Verbale della riunione degli studenti SMERM del 26 marzo 2023

## Ordine del giorno

- situazione della Scuola di Musica Elettronica, cosa fare, come e quando
- richiesta del nuovo piano di studi
- varie ed eventuali

## Introduzione e conoscenza dei ragazzi

- disaccordo pratico tra la direzione dei corsi
- si evince un disaccordo fra maestri

## Punti all'ordine del giorno

### Primo punto all’ordine del giorno

- storia della Scuola di Musica Elettronica di Roma
- ricerca musicale a Roma

### Secondo punto all’ordine del giorno

- cosa accade oggi
    - situazione della polarizzazione di alcuni maestri verso la direzione rispetto agli anni precedenti
- le prospettive del Dottorato di Ricerca

## Impressioni

- risulta necessaria la proposta di una componente studentesca per la trasparenza fra docenti ed allievi e fra docenti e Conservatorio
    - ciò deriva dalla mancata rappresentanza nel consiglio accademico da parte di Musica Elettronica
        - rappresentanza dei docenti
        - rappresentanza degli studenti
- tentativo di smantellamento dell'idea pioneristica della ricerca all'interno del Conservatorio

## Richieste da presentare da parte degli studenti ai docenti

- possibilità di leggere e discutere sui piani di studio che stanno per essere proposti
- possibilità di individuazione di un momento musicale alla fine dell’Anno Accademico per l’ascolto dei brani degli studenti realizzati durante l’anno

## Decisioni prese

- si decide di formare il Collettivo degli Studenti della Scuola di Musica Elettronica di Roma del Conservatorio, Santa Cecilia
- si decide all’unanimità di realizzare una votazione annuale di n.2 rappresentati degli studenti della Scuola di Musica Elettronica di Roma
- si decide per una votazione anonima per avvalorare la veridicità del voto
- sono risultati candidati per la rappresentanza a seguito della proposta degli altri studenti:
    - Ilaria Bava Dante
    - Giulio Romano De Mattia
    - si attendono altri candidati entro le ore 9.00 del 27 marzo 2023
- si procederà alla votazione dei candidati nella fascia oraria 9.00-12.00 del 27 marzo 2023
- i due candidati sono stati proposti per via delle loro qualità ed in particolare è stato evidenziato che:
  - Ilaria frequenta il terzo anno di Triennio ed è ritenuta capace ad esplicare al meglio le proposte e le necessità degli studenti
  - Giulio, in quanto studente "veterano" esperto e a conoscenza delle dinamiche interne alla Scuola di Musica Elettronica di Roma

## Sono stati presenti alla riunione gli studenti

La riunione si è tenuta online presso Google Meet ed erano presenti gli studenti:

- Andrea Geremia
- Daniele Tegas
- Edoardo Scioscia
- Davide Tedesco
- Federico Martino
- Filippo Fossà
- Francesco Ferracuti
- Giovanni Michelangelo D’Urso
- Giulio Romano De Mattia
- Ilaria Bava Dente
- Leonardo Saba
- Luca Simone
- Matteo Savastano
- Thomas Rizzi Pretorius
